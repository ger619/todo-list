defmodule Abolger.Todos.Todo do
  use Ecto.Schema
  import Ecto.Changeset

  alias Abolger.Repo


  alias Abolger.Accounts.User
  alias Abolger.Items.Item

  schema "todos" do
    field :todo, :string
    belongs_to :user, User
    has_many :items, Item

    timestamps()
  end

  def changeset(todo, attrs) do
    todo
    |> cast(attrs, [:todo,  :user_id])
    |> validate_required([:todo])
  end


end
