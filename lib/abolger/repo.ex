defmodule Abolger.Repo do
  use Ecto.Repo,
    otp_app: :abolger,
    adapter: Ecto.Adapters.Postgres
end
