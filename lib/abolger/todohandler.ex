defmodule Abolger.Todohandler do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Abolger.Repo
  alias Abolger.Todos.Todo
  alias Abolger.Accounts.User

  @doc """
  Returns the list of todos.

  ## Examples

      iex> list_todos()
      [%Todo{}, ...]

  """
  def list_todos do
    Repo.all(Todo)
  end

  @doc """
  Gets a single todo.

  Raises `Ecto.NoResultsError` if the Todo does not exist.

  ## Examples

      iex> get_todo!(123)
      %Todo{}

      iex> get_todo!(456)
      ** (Ecto.NoResultsError)

  """
  # def get_todo!(id), do: Repo.get!(Todo, id)
  def get_todo!(id) do
    Todo
    |> Repo.get!(id)
    |> Repo.preload(:items)
  end

  @doc """
  Creates a todo.

  ## Examples

      iex> create_todo(%{field: value})
      {:ok, %Todo{}}

      iex> create_todo(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """

def get_by(%{"todo_id" => todo_id}), do: Repo.get(Todo, todo_id)



def create_todo(%User{} = user, attrs \\ %{}) do
  user
  |> Ecto.build_assoc(:todos)
  |> Todo.changeset(attrs)
  |> Repo.insert()
end

  # def create_todo(attrs \\ %{}) do
  #   %Todo{}
  #   |> Todo.changeset(attrs)
  #   |> Repo.insert()
  # end

  @doc """
  Updates a todo.

  ## Examples

      iex> update_todo(todo, %{field: new_value})
      {:ok, %Todo{}}

      iex> update_todo(todo, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_todo(%User{} = user, attrs \\ %{}) do
    user
    |> Ecto.build_assoc(:todos)
    |> Todo.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Todo.

  ## Examples

      iex> delete_todo(todo)
      {:ok, %Todo{}}

      iex> delete_todo(todo)
      {:error, %Ecto.Changeset{}}

  """
  def delete_todo(%Todo{} = todo) do
    Repo.delete(todo)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking todo changes.

  ## Examples

      iex> change_todo(todo)
      %Ecto.Changeset{source: %Todo{}}

  """
  def change_todo(%Todo{} = todo) do
    Todo.changeset(todo, %{})
  end



end
