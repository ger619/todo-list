defmodule Abolger.Items.Item do
  use Ecto.Schema
  import Ecto.Changeset

  alias Abolger.Todos.Todo

  schema "items" do
    field :item, :string
    belongs_to :todo, Todo



    timestamps()
  end

  @doc false
  def changeset(item, attrs) do
    item
    |> cast(attrs, [:item, :todo_id])
    |> validate_required([:item])
  end
end
