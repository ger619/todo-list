defmodule AbolgerWeb.ItemController do
  use AbolgerWeb, :controller

  alias Abolger.Items
  alias Abolger.Items.Item
  alias Abolger.Todohandler

  def index(conn, _params) do
    items = Items.list_items()
    render(conn, "index.html", items: items)
  end

  def new(conn, _params) do
    changeset = Items.change_item(%Item{})
    render(conn, "new.html", changeset: changeset)
  end


  def create(conn, %{"todo_id" => todo_id, "item" => item_params}) do
    todo = Todohandler.get_todo!(todo_id)

    case Items.create_item(todo, item_params) do
      {:ok, _item} ->
        conn
        |> put_flash(:info, "Item created successfully.")
        |> redirect(to: Routes.todo_path(conn, :show, todo))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end



  def show(conn, %{"id" => id}) do
    todo = Todohandler.get_todo!(id)
    item_changeset = Items.change_item(%Item{})
    render(conn, "show.html", todo: todo, item_changeset: item_changeset)
  end


  def edit(conn, %{"id" => id}) do
    item = Items.get_item!(id)
    changeset = Items.change_item(item)
    render(conn, "edit.html", item: item, changeset: changeset)
  end

  def update(conn, %{"id" => id, "item" => item_params}) do
    item = Items.get_item!(id)

    case Items.update_item(item, item_params) do
      {:ok, item} ->
        conn
        |> put_flash(:info, "Item updated successfully.")
        |> redirect(to: Routes.item_path(conn, :show, item))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", item: item, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    item = Items.get_item!(id)
    {:ok, _item} = Items.delete_item(item)

    conn
    |> put_flash(:info, "Item deleted successfully.")
    |> redirect(to: Routes.item_path(conn, :index))
  end
end
