defmodule AbolgerWeb.PageController do
  use AbolgerWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
  def dashboard(conn, _params) do
    render(conn, "index.html")
  end
end
