defmodule AbolgerWeb.TodoController do
  use AbolgerWeb, :controller

  alias Abolger.Todos.Todo
  alias Abolger.Repo
  alias Abolger.Accounts.User
  alias Abolger.Todohandler
  alias Abolger.Items
  alias Abolger.Items.Item


    def index(conn, _params) do
      todo = Todohandler.list_todos()
      render(conn, "index.html", todos: todo)
    end


    def show(conn, %{"id" => id}) do
      todo = Todohandler.get_todo!(id)
      items = Items.change_item(%Item{})
      render(conn, "show.html", todo: todo, items: items)
    end


      def new(conn, _params) do
        changeset = Todohandler.change_todo(%Todo{})

        render(conn, "new.html", changeset: changeset)
      end

      def create(conn, %{"todo" => todo_params}) do
            case Todohandler.create_todo(conn.assigns.current_user, todo_params) do
              {:ok, _comment} ->
                conn
                |> put_flash(:info, "Todo created successfully.")
                |> redirect(to: Routes.todo_path(conn, :index))
              {:error, _changeset} ->
                conn
                |> put_flash(:error, "Issue creating todo.")
                |> redirect(to: Routes.todo_path(conn, :index))
            end
      end


  

      def edit(conn, %{"id" => todo_id}) do
        todo = Repo.get(Todo, todo_id)
        changeset = Todo.changeset(todo)
        render(conn, "edit.html", changeset: changeset, todo: todo)
      end

      def update(conn, %{"id" => todo_id, "todo" => todo}) do
        old_todo = Repo.get(Todo, todo_id)
        changeset = Todo.changeset(old_todo, todo)

        # changeset = Repo.get(Todo, todo_id) |> Todo.changeset(todo)
        case Repo.update(changeset) do
          {:ok, _todo} ->
            conn
            |> put_flash(:info, "Todo Updated")
            |> redirect(to: Routes.todo_path(conn, :index))

          {:error, changeset} ->
            render(conn, "edit.html", changeset: changeset, todo: old_todo)
        end
      end

      def delete(conn, %{"id" => todo_id}) do
        Repo.get!(Todo, todo_id) |> Repo.delete!()

        conn
        |> put_flash(:info, "Todo Deleted")
        |> redirect(to: Routes.todo_path(conn, :index))
      end

      def check_todo_owner(conn, _params) do
        %{params: %{"id" => todo_id}} = conn

        if Repo.get(Todo, todo_id).user_id == conn.assigns.current_user do
          conn
        else
          conn
          |> put_flash(:error, "Your cannot edit that")
          |> redirect(to: Routes.todo_path(conn, :index))
          |> halt()
        end
      end
    end
