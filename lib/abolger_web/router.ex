defmodule AbolgerWeb.Router do
  use AbolgerWeb, :router

  import AbolgerWeb.Authorize

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Phauxth.Authenticate
    plug Phauxth.Remember, create_session_func: &AbolgerWeb.Auth.Utils.create_session/1

  end

  pipeline :authenticated do
    plug :user_check
  end


  pipeline :guest do
    plug :guest_check
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AbolgerWeb do
    pipe_through [:browser, :guest_check]

    get "/", PageController, :index
    resources "/sessions", SessionController, only: [:new, :create]
    resources "/users", UserController, only: [:new, :create]


  end

  scope "/", AbolgerWeb do
    pipe_through [:browser, :authenticated]
    resources "/users", UserController, except: [:new, :create]
    resources "/sessions", SessionController, only: [:delete]
    get "/dashboard", PageController, :dashboard

    resources "/todos", TodoController do
      resources "/items", ItemController
    end




  end


  # Other scopes may use custom stacks.
  # scope "/api", AbolgerWeb do
  #   pipe_through :api
  # end
end
