defmodule Abolger.Repo.Migrations.CreateItems do
  use Ecto.Migration

  def change do
    create table(:items) do
      add :item, :string
      add :todo_id, references(:todos, on_delete: :nothing)

      timestamps()
    end

    create index(:items, [:todo_id])
  end
end
