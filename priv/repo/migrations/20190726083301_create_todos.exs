defmodule Abolger.Repo.Migrations.CreateTodos do
  use Ecto.Migration

  def change do
    create table(:todos) do
      add :todo, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create unique_index(:todos, [:todo])

  end
end
